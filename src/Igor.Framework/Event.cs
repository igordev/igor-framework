﻿namespace Igor.Framework
{
    public class Event<TEventMessage> : IEvent<TEventMessage>
    {
        public Event(TEventMessage message)
        {
            Message = message;
        }

        public TEventMessage Message { get; }
    }
}
