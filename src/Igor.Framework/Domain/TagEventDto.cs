﻿namespace Igor.Framework.Domain
{
    public class TagEventDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}