﻿using Igor.Gateway.Dtos.Lighting;

namespace Igor.Framework.Domain
{
    public class SpaceLightingChangedEventDto
    {
        public int Level { get; set; }
        public int CCT { get; set; }
        public Behavior Behavior { get; set; }
        public CurveType CurveType { get; set; }
        public int Duration { get; set; }

        public int Kelvin
        {
            get => CCT;
            set => CCT = value;
        }
    }
}