﻿namespace Igor.Framework.Domain
{
    public class TagRenamedEventDto : TagEventDto
    {
        public string OriginalName { get; set; }
    }
}