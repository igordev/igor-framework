﻿namespace Igor.Framework.Domain
{
    public class ActuatorStateChangedEventDto
    {
        public int EntityId { get; set; }
        public string ExternalId { get; set; }
        public string CommandCollectionName { get; set; }
        public string CommandType { get; set; }
        public string Data { get; set; }
    }
}