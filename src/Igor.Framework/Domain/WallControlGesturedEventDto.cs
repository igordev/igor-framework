﻿using Igor.Gateway.Dtos.WallControls;

namespace Igor.Framework.Domain
{
    public class WallControlGesturedEventDto
    {
        public int ButtonId { get; set; }
        public WallControlEventType EventType { get; set; }
    }
}