﻿namespace Igor.Framework.Domain
{
    public enum SpaceTimerState
    {
        Stopped,
        Started
    }
}