﻿namespace Igor.Framework.Domain
{
    public class TagObjectEventDto : TagEventDto
    {
        public string EntityType { get; set; }
        public int EntityId { get; set; }
    }
}