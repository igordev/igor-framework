﻿namespace Igor.Framework
{
    public interface IIdentifiable
    {
        string Id { get; }
    }
}
