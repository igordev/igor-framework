﻿using Igor.Framework.Domain;

namespace Igor.Framework.WebHooks.WallControls
{
    public class WallControlGesturedWebHookNotification : WebHookNotification
    {
        private WallControlGesturedEventDto _data;

        public WallControlGesturedWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public WallControlGesturedEventDto Data => _data ??= ValueToObject<WallControlGesturedEventDto>();
    }
}