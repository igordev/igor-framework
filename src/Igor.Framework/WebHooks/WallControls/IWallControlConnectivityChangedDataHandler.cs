﻿using Igor.Gateway.Dtos.WallControls;

namespace Igor.Framework.WebHooks.WallControls
{
    public interface IWallControlConnectivityChangedDataHandler : IWebHookEventHandler<WallControlDto, WebHookNotification>
    {
    }
}
