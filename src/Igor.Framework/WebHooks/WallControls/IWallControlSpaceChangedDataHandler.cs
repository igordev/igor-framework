﻿using Igor.Gateway.Dtos.WallControls;

namespace Igor.Framework.WebHooks.WallControls
{
    public interface IWallControlSpaceChangedDataHandler : IWebHookEventHandler<WallControlDto, WebHookNotification>
    {
        
    }
}