﻿using Igor.Framework.Mqtt;

namespace Igor.Framework.WebHooks.WallControls
{
    public interface IWallControlGesturedEventHandler : IWebHookEventHandler<WallControlGesturedWebHookNotification>
    {
        
    }
}