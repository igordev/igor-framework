﻿using Igor.Gateway.Dtos.Sensors;

namespace Igor.Framework.WebHooks.Sensors
{
    public interface ISensorSpaceChangedDataHandler : IWebHookEventHandler<SensorDto, WebHookNotification>
    {
    }
}
