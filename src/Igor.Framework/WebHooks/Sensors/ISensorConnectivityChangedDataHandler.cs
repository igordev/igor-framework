﻿using Igor.Gateway.Dtos.Sensors;

namespace Igor.Framework.WebHooks.Sensors
{
    public interface ISensorConnectivityChangedDataHandler : IWebHookEventHandler<SensorDto, WebHookNotification>
    {
        
    }
}