﻿using Igor.Gateway.Dtos.Sensors;

namespace Igor.Framework.WebHooks.Sensors
{
    public interface ISensorStateChangedDataHandler : IWebHookEventHandler<SensorDto, WebHookNotification>
    {
        
    }
}