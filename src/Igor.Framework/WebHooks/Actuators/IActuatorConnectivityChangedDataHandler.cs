﻿using Igor.Gateway.Dtos.Actuators;

namespace Igor.Framework.WebHooks.Actuators
{
    public interface IActuatorConnectivityChangedDataHandler : IWebHookEventHandler<ActuatorDto, WebHookNotification>
    {
        
    }
}