﻿using System.Collections.Generic;
using Igor.Framework.Domain;

namespace Igor.Framework.WebHooks.Actuators
{
    public class ActuatorsStateChangedWebHookNotification : WebHookNotification
    {
        private IList<ActuatorStateChangedEventDto> _data;

        public ActuatorsStateChangedWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public IList<ActuatorStateChangedEventDto> Data => _data ??= ValueToArray<ActuatorStateChangedEventDto>();
    }
}