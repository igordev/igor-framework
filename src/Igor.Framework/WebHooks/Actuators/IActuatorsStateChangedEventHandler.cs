﻿namespace Igor.Framework.WebHooks.Actuators
{
    public interface IActuatorsStateChangedEventHandler : IWebHookEventHandler<ActuatorsStateChangedWebHookNotification>
    {
        
    }
}