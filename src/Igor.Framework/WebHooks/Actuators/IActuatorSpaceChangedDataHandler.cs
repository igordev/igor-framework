﻿using Igor.Gateway.Dtos.Actuators;

namespace Igor.Framework.WebHooks.Actuators
{
    public interface IActuatorSpaceChangedDataHandler : IWebHookEventHandler<ActuatorDto, WebHookNotification>
    {
        
    }
}