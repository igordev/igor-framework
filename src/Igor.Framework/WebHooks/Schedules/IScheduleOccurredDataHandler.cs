﻿using Igor.Gateway.Dtos.Schedules;

namespace Igor.Framework.WebHooks.Schedules
{
    public interface IScheduleOccurredDataHandler : IWebHookEventHandler<ScheduleDto, WebHookNotification>
    {
        
    }
}