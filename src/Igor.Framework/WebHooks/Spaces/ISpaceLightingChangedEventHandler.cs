﻿namespace Igor.Framework.WebHooks.Spaces
{
    public interface ISpaceLightingChangedEventHandler : IWebHookEventHandler<SpaceLightingChangedWebHookNotification>
    {
        
    }
}