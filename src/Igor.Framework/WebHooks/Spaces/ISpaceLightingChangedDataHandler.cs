﻿using Igor.Gateway.Dtos.Spaces;

namespace Igor.Framework.WebHooks.Spaces
{
    public interface ISpaceLightingChangedDataHandler : IWebHookEventHandler<SpaceDto, SpaceLightingChangedWebHookNotification>
    {
        
    }
}