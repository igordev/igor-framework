﻿using Igor.Gateway.Dtos.Spaces;

namespace Igor.Framework.WebHooks.Spaces
{
    public interface ISpaceTimerStoppedDataHandler : IWebHookEventHandler<SpaceDto, WebHookNotification>
    {
        
    }
}