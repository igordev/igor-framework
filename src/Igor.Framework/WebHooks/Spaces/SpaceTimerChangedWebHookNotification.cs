﻿using System;
using Igor.Framework.Domain;

namespace Igor.Framework.WebHooks.Spaces
{
    public class SpaceTimerChangedWebHookNotification : WebHookNotification
    {
        public SpaceTimerChangedWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public SpaceTimerState Data => Action.Equals("Space Timer Started", StringComparison.OrdinalIgnoreCase) ? SpaceTimerState.Started : SpaceTimerState.Stopped;
    }
}