﻿using Igor.Gateway.Dtos.Spaces;

namespace Igor.Framework.WebHooks.Spaces
{
    public interface ISpaceEventDataHandler : IWebHookEventHandler<SpaceDto, WebHookNotification>
    {
        
    }
}