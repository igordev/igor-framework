﻿namespace Igor.Framework.WebHooks.Spaces
{
    public interface ISpaceTimerChangedEventHandler : IWebHookEventHandler<SpaceTimerChangedWebHookNotification>
    {
        
    }
}