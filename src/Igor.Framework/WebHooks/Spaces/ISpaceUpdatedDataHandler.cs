﻿using Igor.Gateway.Dtos.Spaces;

namespace Igor.Framework.WebHooks.Spaces
{
    public interface ISpaceUpdatedDataHandler : IWebHookEventHandler<SpaceDto, WebHookNotification>
    {
        
    }
}