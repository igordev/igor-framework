﻿using Igor.Gateway.Dtos.Spaces;

namespace Igor.Framework.WebHooks.Spaces
{
    public interface ISpaceStateChangedDataHandler : IWebHookEventHandler<SpaceDto, WebHookNotification>
    {
        
    }
}