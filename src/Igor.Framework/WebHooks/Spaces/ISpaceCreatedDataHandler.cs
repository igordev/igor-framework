﻿using Igor.Gateway.Dtos.Spaces;

namespace Igor.Framework.WebHooks.Spaces
{
    public interface ISpaceCreatedDataHandler : IWebHookEventHandler<SpaceDto, WebHookNotification>
    {
        
    }
}