﻿using Igor.Gateway.Dtos.Spaces;

namespace Igor.Framework.WebHooks.Spaces
{
    public interface ISpaceTimerChangedDataHandler : IWebHookEventHandler<SpaceDto, SpaceTimerChangedWebHookNotification>
    {
        
    }
}