﻿using Igor.Framework.Domain;

namespace Igor.Framework.WebHooks.Spaces
{
    public class SpaceLightingChangedWebHookNotification : WebHookNotification
    {
        private SpaceLightingChangedEventDto _data;

        public SpaceLightingChangedWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public SpaceLightingChangedEventDto Data => _data ?? (_data = ValueToObject<SpaceLightingChangedEventDto>());
    }
}