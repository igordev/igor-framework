﻿namespace Igor.Framework.WebHooks.Tags
{
    public interface ITagsCreatedEventHandler : IWebHookEventHandler<TagWebHookNotification>
    {
    }
}