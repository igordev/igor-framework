﻿namespace Igor.Framework.WebHooks.Tags
{
    public interface ITagsAddedToObjectsEventHandler : IWebHookEventHandler<TagObjectWebHookNotification>
    {
        
    }
}