﻿namespace Igor.Framework.WebHooks.Tags
{
    public interface ITagsDeletedEventHandler : IWebHookEventHandler<TagWebHookNotification>
    {
        
    }
}