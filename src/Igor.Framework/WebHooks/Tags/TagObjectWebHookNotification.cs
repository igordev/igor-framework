﻿using System.Collections.Generic;
using Igor.Framework.Domain;

namespace Igor.Framework.WebHooks.Tags
{
    public class TagObjectWebHookNotification : WebHookNotification
    {
        private IList<TagObjectEventDto> _data;

        public TagObjectWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public IList<TagObjectEventDto> Data => _data ??= ValueToArray<TagObjectEventDto>();
    }
}