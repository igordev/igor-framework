﻿namespace Igor.Framework.WebHooks.Tags
{
    public interface ITagsRenamedEventHandler : IWebHookEventHandler<TagRenamedWebHookNotification>
    {
        
    }
}