﻿using System.Collections.Generic;
using Igor.Framework.Domain;

namespace Igor.Framework.WebHooks.Tags
{
    public class TagWebHookNotification : WebHookNotification
    {
        private IList<TagEventDto> _data;

        public TagWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public IList<TagEventDto> Data => _data ?? (_data = ValueToArray<TagEventDto>());
    }
}