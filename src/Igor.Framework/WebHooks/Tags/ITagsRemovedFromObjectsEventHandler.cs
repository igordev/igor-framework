﻿namespace Igor.Framework.WebHooks.Tags
{
    public interface ITagsRemovedFromObjectsEventHandler : IWebHookEventHandler<TagObjectWebHookNotification>
    {
        
    }
}