﻿using System.Collections.Generic;
using Igor.Framework.Domain;

namespace Igor.Framework.WebHooks.Tags
{
    public class TagRenamedWebHookNotification : WebHookNotification
    {
        private IList<TagRenamedEventDto> _data;

        public TagRenamedWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public IList<TagRenamedEventDto> Data => _data ?? (_data = ValueToArray<TagRenamedEventDto>());
    }
}