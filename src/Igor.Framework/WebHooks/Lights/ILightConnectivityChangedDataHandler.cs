﻿using Igor.Gateway.Dtos.Lights;

namespace Igor.Framework.WebHooks.Lights
{
    public interface ILightConnectivityChangedDataHandler : IWebHookEventHandler<LightDto, WebHookNotification>
    {
    }
}
