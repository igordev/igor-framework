﻿using Igor.Gateway.Dtos.Lights;

namespace Igor.Framework.WebHooks.Lights
{
    public interface ILightSpaceChangedDataHandler : IWebHookEventHandler<LightDto, WebHookNotification>
    {
        
    }
}