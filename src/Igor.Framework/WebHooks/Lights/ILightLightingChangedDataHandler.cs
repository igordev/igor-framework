﻿using Igor.Gateway.Dtos.Lights;

namespace Igor.Framework.WebHooks.Lights
{
    public interface ILightLightingChangedDataHandler : IWebHookEventHandler<LightDto, LightLightingChangedWebHookNotification>
    {
        
    }
}