﻿using System.Collections.Generic;
using System.Linq;
using Igor.Gateway.Dtos.Lights;

namespace Igor.Framework.WebHooks.Lights
{
    public class LightsLightingChangedWebHookNotification : WebHookNotification
    {
        private IList<LightsLightingChangedEventDto> _data;

        public LightsLightingChangedWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public IList<LightsLightingChangedEventDto> Data => _data ?? (_data = ValueToArray<LightsLightingChangedEventDto>());
    }
}