﻿namespace Igor.Framework.WebHooks.Lights
{
    public interface ILightsLightingChangedEventHandler : IWebHookEventHandler<LightsLightingChangedWebHookNotification>
    {
        
    }
}