﻿namespace Igor.Framework.WebHooks.Lights
{
    public interface ILightLightingChangedEventHandler : IWebHookEventHandler<LightLightingChangedWebHookNotification>
    {
        
    }
}