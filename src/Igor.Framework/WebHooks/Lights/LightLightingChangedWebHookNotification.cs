﻿using Igor.Gateway.Api.Sdk.Apis.Lights;

namespace Igor.Framework.WebHooks.Lights
{
    public class LightLightingChangedWebHookNotification : WebHookNotification
    {
        private LightLightingChangedEventDto _data;

        public LightLightingChangedWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public LightLightingChangedEventDto Data => _data ?? (_data = ValueToObject<LightLightingChangedEventDto>());
    }
}