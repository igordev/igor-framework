﻿using System.Collections.Generic;

namespace Igor.Framework.WebHooks.Relays
{
    public class RelaysStateChangedWebHookNotification : WebHookNotification
    {
        private IList<RelayStateChangedEventDto> _data;

        public RelaysStateChangedWebHookNotification(WebHookNotification notification) : base(notification)
        {
        }

        public IList<RelayStateChangedEventDto> Data => _data ?? (_data = ValueToArray<RelayStateChangedEventDto>());
    }
}