﻿using Igor.Gateway.Dtos.Relays;

namespace Igor.Framework.WebHooks.Relays
{
    public interface IRelayConnectivityChangedDataHandler : IWebHookEventHandler<RelayDto, WebHookNotification>
    {
        
    }
}