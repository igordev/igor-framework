﻿using Igor.Gateway.Dtos.Relays;

namespace Igor.Framework.WebHooks.Relays
{
    public interface IRelaySpaceChangedDataHandler : IWebHookEventHandler<RelayDto, WebHookNotification>
    {
        
    }
}