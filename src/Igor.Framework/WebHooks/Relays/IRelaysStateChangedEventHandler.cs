﻿namespace Igor.Framework.WebHooks.Relays
{
    public interface IRelaysStateChangedEventHandler : IWebHookEventHandler<RelaysStateChangedWebHookNotification>
    {
        
    }
}