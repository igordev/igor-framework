﻿using Igor.Gateway.Dtos.Relays;

namespace Igor.Framework.WebHooks.Relays
{
    public class RelayStateChangedEventDto
    {
        public int EntityId { get; set; }

        public string ExternalId { get; set; }

        public RelayState RelayState { get; set; }
    }
}