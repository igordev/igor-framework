﻿using Igor.Gateway.Dtos.Relays;

namespace Igor.Framework.WebHooks.Relays
{
    public interface IRelayStateChangedDataHandler : IWebHookEventHandler<RelayDto, WebHookNotification>
    {
        
    }
}