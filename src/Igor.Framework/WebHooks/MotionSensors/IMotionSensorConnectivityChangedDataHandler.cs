﻿using Igor.Gateway.Dtos.MotionSensors;

namespace Igor.Framework.WebHooks.MotionSensors
{
    public interface IMotionSensorConnectivityChangedDataHandler : IWebHookEventHandler<MotionSensorDto, WebHookNotification>
    {
        
    }
}