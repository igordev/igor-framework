﻿using Igor.Gateway.Dtos.MotionSensors;

namespace Igor.Framework.WebHooks.MotionSensors
{
    public interface IMotionSensorSpaceChangedDataHandler : IWebHookEventHandler<MotionSensorDto, WebHookNotification>
    {
        
    }
}