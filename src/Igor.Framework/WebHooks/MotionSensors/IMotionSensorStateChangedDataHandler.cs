﻿using Igor.Gateway.Dtos.MotionSensors;

namespace Igor.Framework.WebHooks.MotionSensors
{
    public interface IMotionSensorStateChangedDataHandler : IWebHookEventHandler<MotionSensorDto, WebHookNotification>
    {
        
    }
}