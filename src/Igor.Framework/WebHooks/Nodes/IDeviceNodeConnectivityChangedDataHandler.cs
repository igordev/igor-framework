﻿using Igor.Gateway.Dtos.DeviceNodes;

namespace Igor.Framework.WebHooks.Nodes
{
    public interface IDeviceNodeConnectivityChangedDataHandler : IWebHookEventHandler<DeviceNodeDto, WebHookNotification>
    {
        
    }
}