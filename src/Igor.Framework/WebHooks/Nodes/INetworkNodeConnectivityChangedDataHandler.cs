﻿using Igor.Gateway.Dtos.NetworkNodes;

namespace Igor.Framework.WebHooks.Nodes
{
    public interface INetworkNodeConnectivityChangedDataHandler : IWebHookEventHandler<NetworkNodeDto, WebHookNotification>
    {
        
    }
}