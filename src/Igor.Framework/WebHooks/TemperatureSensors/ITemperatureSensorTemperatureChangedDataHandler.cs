﻿using Igor.Gateway.Dtos.TemperatureSensors;

namespace Igor.Framework.WebHooks.TemperatureSensors
{
    public interface ITemperatureSensorTemperatureChangedDataHandler : IWebHookEventHandler<TemperatureSensorDto, WebHookNotification>
    {
        
    }
}