﻿using Igor.Gateway.Dtos.TemperatureSensors;

namespace Igor.Framework.WebHooks.TemperatureSensors
{
    public interface ITemperatureSensorConnectivityChangedDataHandler : IWebHookEventHandler<TemperatureSensorDto, WebHookNotification>
    {
        
    }
}