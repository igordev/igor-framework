﻿using Igor.Gateway.Dtos.TemperatureSensors;

namespace Igor.Framework.WebHooks.TemperatureSensors
{
    public interface ITemperatureSensorSpaceChangedDataHandler : IWebHookEventHandler<TemperatureSensorDto, WebHookNotification>
    {
        
    }
}