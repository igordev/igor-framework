﻿using Igor.Gateway.Dtos.Zones;

namespace Igor.Framework.WebHooks.Zones
{
    public interface IZoneLevelChangedDataHandler : IWebHookEventHandler<ZoneDto, WebHookNotification>
    {
        
    }
}