﻿namespace Igor.Framework.WebHooks
{
    public interface IWebHookEventHandler : IWebHookEventHandler<WebHookNotification>
    {
    }

    public interface IWebHookEventHandler<in TEventMessage> : IEventHandler<IEvent<TEventMessage>> where TEventMessage : WebHookNotification
    {
    }

    public interface IWebHookEventHandler<in TData, in TEventMessage> : IEventHandler<TData, TEventMessage> where TEventMessage : WebHookNotification
    {
    }
}
