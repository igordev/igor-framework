﻿using Igor.Gateway.Dtos.DryContacts;

namespace Igor.Framework.WebHooks.DryContacts
{
    public interface IDryContactStateChangedDataHandler : IWebHookEventHandler<DryContactDto, WebHookNotification>
    {
        
    }
}