﻿using Igor.Gateway.Dtos.DryContacts;

namespace Igor.Framework.WebHooks.DryContacts
{
    public interface IDryContactSpaceChangedDataHandler : IWebHookEventHandler<DryContactDto, WebHookNotification>
    {
        
    }
}