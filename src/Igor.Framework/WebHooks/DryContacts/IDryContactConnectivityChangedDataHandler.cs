﻿using Igor.Gateway.Dtos.DryContacts;

namespace Igor.Framework.WebHooks.DryContacts
{
    public interface IDryContactConnectivityChangedDataHandler : IWebHookEventHandler<DryContactDto, WebHookNotification>
    {
        
    }
}