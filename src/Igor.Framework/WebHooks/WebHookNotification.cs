﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Igor.Framework.WebHooks
{
    public class WebHookNotification : IIdentifiable
    {
        public WebHookNotification(WebHookEvent webHookEvent)
        {
            EventId = webHookEvent.Id;
            EventAttempt = webHookEvent.Attempt;
            EventReceived = webHookEvent.EventReceived;
        }

        public WebHookNotification(WebHookNotification notification)
        {
            EventId = notification.EventId;
            EventAttempt = notification.EventAttempt;
            EventReceived = notification.EventReceived;
            Action = notification.Action;
            Entity = notification.Entity;
            EntityId = notification.EntityId;
            Value = notification.Value;
            ExternalId = notification.ExternalId;
            SpaceId = notification.SpaceId;
        }

        public string EventId { get; }

        public int EventAttempt { get; }

        public DateTime EventReceived { get; }

        public string Id => EntityId.ToString();

        public string Action { get; set; }

        public string Entity { get; set; }

        public int EntityId { get; set; }

        public object Value { get; set; }

        public string ExternalId { get; set; }

        public int? SpaceId { get; set; }

        public T ValueToObject<T>()
        {
            if (Value is string str)
                return JsonConvert.DeserializeObject<T>(str);

            return JObject.FromObject(Value).ToObject<T>();
        }

        public T[] ValueToArray<T>()
        {
            if (Value is string str)
                return JsonConvert.DeserializeObject<T[]>(str);

            return JArray.FromObject(Value).Children().Select(t => t.ToObject<T>()).ToArray<T>();
        }

        public string ValueAsString(string @default = null)
        {
            if (!(Value is string str))
                return @default;

            return str;
        }

        public int? ValueAsInt(int? @default = null)
        {
            if (!int.TryParse(Value as string, out var result))
                return @default;

            return result;
        }

        public double? ValueAsDouble(double? @default = null)
        {
            if (!double.TryParse(Value as string, out var result))
                return @default;

            return result;
        }
    }
}
