﻿using Igor.Gateway.Dtos.LightSensors;

namespace Igor.Framework.WebHooks.LightSensors
{
    public interface ILightSensorConnectivityChangedDataHandler : IWebHookEventHandler<LightSensorDto, WebHookNotification>
    {
    }
}
