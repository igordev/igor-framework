﻿using Igor.Gateway.Dtos.LightSensors;

namespace Igor.Framework.WebHooks.LightSensors
{
    public interface ILightSensorIlluminanceChangedDataHandler : IWebHookEventHandler<LightSensorDto, WebHookNotification>
    {
        
    }
}