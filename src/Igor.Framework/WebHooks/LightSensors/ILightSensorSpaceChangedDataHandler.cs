﻿using Igor.Gateway.Dtos.LightSensors;

namespace Igor.Framework.WebHooks.LightSensors
{
    public interface ILightSensorSpaceChangedDataHandler : IWebHookEventHandler<LightSensorDto, WebHookNotification>
    {
        
    }
}