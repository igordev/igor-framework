﻿using Igor.Gateway.Dtos.ActionSets;

namespace Igor.Framework.WebHooks.ActionSets
{
    public interface IActionSetCancelledDataHandler : IWebHookEventHandler<ActionSetDto, WebHookNotification>
    {
    }
}
