﻿using Igor.Gateway.Dtos.ActionSets;

namespace Igor.Framework.WebHooks.ActionSets
{
    public interface IActionSetCompletedDataHandler : IWebHookEventHandler<ActionSetDto, WebHookNotification>
    {
    }
}
