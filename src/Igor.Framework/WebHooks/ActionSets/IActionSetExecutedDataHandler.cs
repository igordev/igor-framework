﻿using Igor.Gateway.Dtos.ActionSets;

namespace Igor.Framework.WebHooks.ActionSets
{
    public interface IActionSetExecutedDataHandler : IWebHookEventHandler<ActionSetDto, WebHookNotification>
    {
    }
}
