﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Igor.Framework.WebHooks
{
    public class WebHookEvent
    {
        public WebHookEvent()
        {
            EventReceived = DateTime.Now;
            Notifications = new List<WebHookNotification>();
        }

        public WebHookEvent(IEnumerable<WebHookNotification> notifications)
            : this()
        {
            Notifications = notifications.ToList();
        }

        public string Id { get; set; }

        public int Attempt { get; set; }

        public DateTime EventReceived { get; }

        public IList<WebHookNotification> Notifications { get; set; }
    }
}
