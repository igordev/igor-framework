﻿using System.Threading.Tasks;

namespace Igor.Framework
{
    public interface IHttpContextMiddleware
    {
        Task Invoke(Microsoft.AspNetCore.Http.HttpContext context);
    }
}