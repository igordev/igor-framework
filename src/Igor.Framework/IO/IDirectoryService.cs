﻿namespace Igor.Framework.IO
{
    public interface IDirectoryService
    {
        string GetCurrentDirectory();
    }
}