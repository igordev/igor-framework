﻿namespace Igor.Framework
{
    public interface IEvent<out TEventMessage>
    {
        TEventMessage Message { get; }
    }
}