﻿using System.Threading;
using System.Threading.Tasks;

namespace Igor.Framework.Services
{
    public interface ITask
    {
        uint ExecutionFrequencySeconds { get; }
        uint StartupDelaySeconds { get; }
        Task Run(CancellationToken cancellationToken);
    }
}