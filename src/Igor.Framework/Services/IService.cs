﻿namespace Igor.Framework.Services
{
    public interface IService
    {
        void Start();
        void Stop();
    }
}