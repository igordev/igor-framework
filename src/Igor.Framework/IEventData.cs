﻿namespace Igor.Framework
{
    public interface IEventData<out TData, out TEventMessage> : IEvent<TEventMessage>
    {
        TData Data { get; }
    }
}