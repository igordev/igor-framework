﻿using Microsoft.Extensions.Configuration;

namespace Igor.Framework
{
    public interface IPlugin
    {
        string Name { get; }

        void Initialize();

        IConfiguration Configuration { get; }
    }
}