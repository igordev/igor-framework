﻿namespace Igor.Framework.Mqtt.WallControls
{
    public interface IWallControlGesturedEventHandler : IMqttEventHandler<WallControlGesturedMqttEvent>
    {
        
    }
}