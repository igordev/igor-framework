﻿using Igor.Framework.Domain;

namespace Igor.Framework.Mqtt.WallControls
{
    public class WallControlGesturedMqttEvent : MqttEvent
    {
        private WallControlGesturedEventDto _data;

        public WallControlGesturedMqttEvent(MqttEvent notification) : base(notification)
        {
        }

        public WallControlGesturedEventDto Data => _data ??= ValueToObject<WallControlGesturedEventDto>();
    }
}