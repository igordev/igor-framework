﻿namespace Igor.Framework.Mqtt
{
    public class MqttEventDto
    {
        public MqttEventDto()
        {
            Metadata = new Metadata();
            Event = new DomainEvent();
        }

        public Metadata Metadata { get; set; }
        public DomainEvent Event { get; set; }
    }
}