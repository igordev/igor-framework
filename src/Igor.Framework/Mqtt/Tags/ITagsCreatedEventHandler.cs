﻿namespace Igor.Framework.Mqtt.Tags
{
    public interface ITagsCreatedEventHandler : IMqttEventHandler<TagMqttEvent>
    {
    }
}