﻿using System.Collections.Generic;
using Igor.Framework.Domain;

namespace Igor.Framework.Mqtt.Tags
{
    public class TagMqttEvent : MqttEvent
    {
        private IList<TagEventDto> _data;

        public TagMqttEvent(MqttEvent notification) : base(notification)
        {
        }

        public IList<TagEventDto> Data => _data ??= ValueToArray<TagEventDto>();
    }
}