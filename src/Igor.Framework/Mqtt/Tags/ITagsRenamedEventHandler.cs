﻿namespace Igor.Framework.Mqtt.Tags
{
    public interface ITagsRenamedEventHandler : IMqttEventHandler<TagRenamedMqttEvent>
    {
        
    }
}