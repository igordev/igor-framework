﻿using System.Collections.Generic;
using Igor.Framework.Domain;

namespace Igor.Framework.Mqtt.Tags
{
    public class TagRenamedMqttEvent : MqttEvent
    {
        private IList<TagRenamedEventDto> _data;

        public TagRenamedMqttEvent(MqttEvent notification) : base(notification)
        {
        }

        public IList<TagRenamedEventDto> Data => _data ??= ValueToArray<TagRenamedEventDto>();
    }
}