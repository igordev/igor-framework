﻿namespace Igor.Framework.Mqtt.Tags
{
    public interface ITagsAddedToObjectsEventHandler : IMqttEventHandler<TagObjectMqttEvent>
    {
        
    }
}