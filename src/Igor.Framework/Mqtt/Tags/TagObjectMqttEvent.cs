﻿using System.Collections.Generic;
using Igor.Framework.Domain;

namespace Igor.Framework.Mqtt.Tags
{
    public class TagObjectMqttEvent : MqttEvent
    {
        private IList<TagObjectEventDto> _data;

        public TagObjectMqttEvent(MqttEvent notification) : base(notification)
        {
        }

        public IList<TagObjectEventDto> Data => _data ??= ValueToArray<TagObjectEventDto>();
    }
}