﻿namespace Igor.Framework.Mqtt.Tags
{
    public interface ITagsRemovedFromObjectsEventHandler : IMqttEventHandler<TagObjectMqttEvent>
    {
        
    }
}