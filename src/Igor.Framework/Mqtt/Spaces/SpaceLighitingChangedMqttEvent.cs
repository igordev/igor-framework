﻿using Igor.Framework.Domain;

namespace Igor.Framework.Mqtt.Spaces
{
    public class SpaceLightingChangedMqttEvent : MqttEvent
    {
        private SpaceLightingChangedEventDto _data;

        public SpaceLightingChangedMqttEvent(MqttEvent @event) : base(@event)
        {
        }

        public SpaceLightingChangedEventDto Data => _data ??= ValueToObject<SpaceLightingChangedEventDto>();
    }
}

