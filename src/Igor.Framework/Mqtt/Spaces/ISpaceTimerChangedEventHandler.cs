﻿namespace Igor.Framework.Mqtt.Spaces
{
    public interface ISpaceTimerChangedEventHandler : IMqttEventHandler<SpaceTimerChangedMqttEvent>
    {
        
    }
}