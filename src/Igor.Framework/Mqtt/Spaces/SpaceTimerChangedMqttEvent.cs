﻿using System;
using Igor.Framework.Domain;

namespace Igor.Framework.Mqtt.Spaces
{
    public class SpaceTimerChangedMqttEvent : MqttEvent
    {
        public SpaceTimerChangedMqttEvent(MqttEvent notification) : base(notification)
        {
        }

        public SpaceTimerState Data => Action.Equals("Space Timer Started", StringComparison.OrdinalIgnoreCase) ? SpaceTimerState.Started : SpaceTimerState.Stopped;
    }
}