﻿using Igor.Framework.WebHooks;
using Igor.Framework.WebHooks.Spaces;

namespace Igor.Framework.Mqtt.Spaces
{
    public interface ISpaceLightingChangedEventHandler : IMqttEventHandler<SpaceLightingChangedMqttEvent>
    {
        
    }
}