﻿namespace Igor.Framework.Mqtt
{
    public interface IMqttEventHandler : IMqttEventHandler<MqttEvent>
    {

    }

    public interface IMqttEventHandler<in TEventMessage> : IEventHandler<IEvent<TEventMessage>> where TEventMessage : MqttEvent
    {
    }
}