﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Igor.Framework.Mqtt
{
    public class MqttEvent
    {
        public MqttEvent()
        {
            EventReceived = DateTime.Now;
        }

        public MqttEvent(MqttEvent @event)
        {
            TraceId = @event.TraceId;
            EventReceived = @event.EventReceived;
            Action = @event.Action;
            Entity = @event.Entity;
            EntityId = @event.EntityId;
            ExternalId = @event.ExternalId;
            Value = @event.Value;
        }

        public string TraceId { get; set; }

        public DateTime EventReceived { get; }

        public string Action { get; set; }

        public string Entity { get; set; }

        public int EntityId { get; set; }

        public string ExternalId { get; set; }

        public object Value { get; set; }

        public T[] ValueToArray<T>()
        {
            if (Value is string str)
                return JsonConvert.DeserializeObject<T[]>(str);

            return JArray.FromObject(Value).Children().Select(t => t.ToObject<T>()).ToArray<T>();
        }

        public T ValueToObject<T>()
        {
            if (Value is string str)
                return JsonConvert.DeserializeObject<T>(str);

            return JObject.FromObject(Value).ToObject<T>();
        }
    }
}