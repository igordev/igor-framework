﻿namespace Igor.Framework.Mqtt
{
    public class DomainEvent
    {
        public string Action { get; set; }
        public string Entity { get; set; }
        public int EntityId { get; set; }
        public string ExternalId { get; set; }
        public object Value { get; set; }
    }
}