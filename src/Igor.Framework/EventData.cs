﻿namespace Igor.Framework
{
    public class EventData<TData, TMessage> : Event<TMessage>, IEventData<TData, TMessage>
    {
        public EventData(TData data, IEvent<TMessage> @event) 
            : base(@event.Message)
        {
            Data = data;
        }

        public TData Data { get; }
    }
}
