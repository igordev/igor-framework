﻿using System.Threading.Tasks;

namespace Igor.Framework
{
    public interface IEventHandler<in TData, in TEventMessage> : IEventHandler<IEventData<TData, TEventMessage>>
    {
    }

    public interface IEventHandler<in T>
    {
        Task Handle(T @object);
    }
}
