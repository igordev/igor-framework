﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Razor;

namespace Igor.Framework.Web
{
    public class PluginViewLocationExpander : IViewLocationExpander
    {
        private const string PluginAssemblyKey = "plugin";

        public void PopulateValues(ViewLocationExpanderContext context)
        {
            var controller = context.ActionContext.ActionDescriptor.DisplayName;
            var controllersIndex = controller.IndexOf("Controllers", StringComparison.Ordinal);
            var assemblyName = controller.Substring(0, controllersIndex);

            if (assemblyName.EndsWith("."))
                assemblyName = assemblyName.Substring(0, assemblyName.LastIndexOf('.'));

            if (!controller.Contains("Igor.Framework"))
            {
                context.Values[PluginAssemblyKey] = assemblyName;
            }
        }

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (!context.Values.ContainsKey(PluginAssemblyKey)) return viewLocations;

            var assemblyName = context.Values[PluginAssemblyKey];

            if (string.IsNullOrWhiteSpace(assemblyName)) return viewLocations;

            var moduleViewLocations = new[]
            {
                $"/{assemblyName}.Views/{{1}}/{{0}}.cshtml",
                $"/{assemblyName}.Views/Shared/{{0}}.cshtml",
            };

            viewLocations = moduleViewLocations.Concat(viewLocations);

            return viewLocations;
        }
    }
}