﻿using System;

namespace Igor.Framework.Web
{
    [AttributeUsage(AttributeTargets.Class)]
    public class IgorFrameworkRouteAttribute : Attribute
    {
        public string Route { get; }

        public IgorFrameworkRouteAttribute(string route = null)
        {
            Route = route;
        }
    }
}