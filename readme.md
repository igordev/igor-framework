## Overview

The Igor Framework allows developers to quickly create plugins that can extend the functionality of the Igor Gateway Software.

## Igor Framework Features
* Host multiple plugins on the same machine
* Create API endpoints to consume from other applications
* Create web interfaces for users to interact with.
* Automatic Igor Gateway Software API integration. The Igor Framework will automatically scan your plugin for supported interfaces and register web hooks with the gateway.
* Task support to execute your custom code on a fixed interval.
* Support for custom long-running code execution.

### [Click here](https://bitbucket.org/igordev/igor-framework/wiki/Home) to view tutorials on the wiki and get started.