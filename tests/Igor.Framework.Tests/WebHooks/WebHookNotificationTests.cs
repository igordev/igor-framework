﻿using System.Linq;
using Igor.Framework.WebHooks;
using Igor.Framework.WebHooks.Lights;
using Xunit;

namespace Igor.Framework.Tests.WebHooks
{
    public class WebHookNotificationTests
    {
        [Fact]
        public void TestShouldConvertJsonToArray()
        {
            var notification = CreateNotification();

            notification.Value = GetJsonArray();

            var lightsLightingChangedWebHookNotification = new LightsLightingChangedWebHookNotification(notification);

            Assert.Equal(1, lightsLightingChangedWebHookNotification.Data.First().EntityId);
        }

        protected virtual WebHookNotification CreateNotification()
        {
            return new WebHookNotification(new WebHookEvent())
            {
                Action = "LightsLightingChanged"
            };
        }

        private string GetJsonArray()
        {
            return "[" +
                   "{" +
                   "    \"EntityId\": 1," +
                   "    \"ExternalId\": \"test\"," +
                   "    \"Lighting\": {" +
                   "        \"Level\": 4100," +
                   "        \"CCT\": 1000," +
                   "        \"Behavior\": 0," +
                   "        \"CurveType\": 1," +
                   "        \"Duration\": 2000," +
                   "        \"State\": 0" +
                   "    }" +
                   "}" +
                   "]";
        }
    }
}
