﻿using System.Collections.Generic;
using System.Linq;
using Igor.Framework.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Razor;
using Xunit;

namespace Igor.Framework.Tests.Web
{
    public class PluginViewLocationExpanderTests
    {
        private const string AssemblyName = "MyPluginAssembly";

        private readonly ViewLocationExpanderContext _context;
        private readonly PluginViewLocationExpander _expander;

        public PluginViewLocationExpanderTests()
        {
            var actionContext = new ActionContext
            {
                ActionDescriptor = new ActionDescriptor
                {
                    DisplayName = $"{AssemblyName}.Controllers.Home.Index"
                }
            };

            _context = new ViewLocationExpanderContext(actionContext, "Index", "Home", null, null, false)
            {
                Values = new Dictionary<string, string>()
            };

            _expander = new PluginViewLocationExpander();
        }

        [Fact]
        public void TestShouldAddAssemblyNameToContextValues()
        {
            _expander.PopulateValues(_context);

            var value = _context.Values.First();

            Assert.Equal("plugin", value.Key);
            Assert.Equal("MyPluginAssembly", value.Value);
        }

        [Fact]
        public void TestShouldReturnExpandedViewLocations()
        {
            _expander.PopulateValues(_context);

            var locations = _expander.ExpandViewLocations(_context, new List<string>()).ToList();

            var location1 = locations.First();
            var location2 = locations.Last();

            Assert.Equal($"/{AssemblyName}.Views/{{1}}/{{0}}.cshtml", location1);
            Assert.Equal($"/{AssemblyName}.Views/Shared/{{0}}.cshtml", location2);
        }
    }
}