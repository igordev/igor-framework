﻿using System.Threading.Tasks;
using Igor.Framework;
using Igor.Framework.WebHooks;
using Igor.Framework.WebHooks.ActionSets;
using Microsoft.Extensions.Logging;

namespace Logging
{
    public class MyActionSetExecutedEventHandler : IActionSetExecutedEventHandler
    {
        private readonly ILogger _logger;

        // Once you've configured your IConfiguration property in your
        // Initialize() method of your IPlugin implementation, you
        // can simply inject Microsoft.Extensions.Logging.ILogger
        // into the constructor of any of your classes that are called
        // from the Igor Framework.
        //
        // You can configure your log level in your appsettings.json.
        // Valid log levels:
        //   - Debug
        //   - Info
        //   - Warn
        //   - Error
        //   - Fatal
        //
        // See appsettings.json with this project for an example
        public MyActionSetExecutedEventHandler(ILogger logger)
        {
            _logger = logger;
        }

        public Task Handle(IEvent<WebHookNotification> @object)
        {
            // Execute your code here

            // Log files can be found in your plugin directory \Logs.
            _logger.LogInformation($"Processing {@object.Message.Action} Event...");

            // Be sure to return a task
            return Task.CompletedTask;
        }
    }
}