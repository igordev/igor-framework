﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Igor.Framework.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Services
{
    // Services are intended to be used to host long-running
    // operations on their own threads.
    //
    // This example demonstrates starting a UDP listener and
    // listening for incoming data.
    //
    // Make sure you don't have a blocking call in your
    // Start() method. If you do, it could prevent the 
    // Igor Framework Runtime Service from starting.
    public class MyLongRunningService : IService
    {
        private const int ReceiveBufferSize = 1024;

        private readonly ILogger _logger;
        private readonly IPEndPoint _endpoint;

        private UdpClient _client;
        private CancellationTokenSource _cancellationTokenSource;
        private bool _isRunning = true;

        public MyLongRunningService(ILogger logger, IConfiguration configuration)
        {
            _logger = logger;

            var ipAddress = configuration["ExampleUDPEndpoint"];

            _endpoint = new IPEndPoint(IPAddress.Parse(ipAddress), 6000);
        }

        // Start is called when the Igor Framework Runtime Service starts
        public void Start()
        {
            _logger.LogInformation("Starting My Long Running Service");

            _client = new UdpClient();
            
            _client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            _client.ExclusiveAddressUse = false;
            _client.Client.ReceiveBufferSize = ReceiveBufferSize;
            _client.Client.Bind(_endpoint);
            _cancellationTokenSource = new CancellationTokenSource();

            Task.Run(Listen, _cancellationTokenSource.Token);
        }

        // Stop is called when the Igor Framework Runtime Service stops
        public void Stop()
        {
            try
            {
                _isRunning = false;

                _cancellationTokenSource.Cancel();
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                _client.Dispose();
            }
        }

        private async Task Listen()
        {
            while (_isRunning)
            {
                try
                {
                    var result = await _client.ReceiveAsync();

                    // do something with result when data is received
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Something went wrong!");
                }
            }
        }
    }
}
