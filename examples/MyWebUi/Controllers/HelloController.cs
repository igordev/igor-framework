﻿using Igor.Framework.Web;
using Microsoft.AspNetCore.Mvc;

namespace MyWebUi.Controllers
{
    [IgorFrameworkRoute]
    public class HelloController : Controller
    {
        [HttpGet]
        [Route("")]
        public IActionResult Index([FromQuery] string echo)
        {
            ViewData["echo"] = echo;

            return View();
        }
    }
}