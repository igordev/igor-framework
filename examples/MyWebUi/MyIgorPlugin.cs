﻿using Igor.Framework;
using Igor.Framework.IO;
using Microsoft.Extensions.Configuration;

namespace MyWebUi
{
    public class MyIgorPlugin : IPlugin
    {
        private readonly IDirectoryService _directoryService;

        // Parameterless constructor is required
        public MyIgorPlugin()
        {
        }

        // Inject the IDirectoryService into any of your classes
        // if you need to access your plugins directory on
        // the file system.
        public MyIgorPlugin(IDirectoryService directoryService)
        {
            _directoryService = directoryService;
        }

        // You can run any initialization code your plugin may need to execute
        // before starting in the Initialize() method.
        // This is also where you should setup your configuration
        // to be used by your other classes and logging.
        public void Initialize()
        {
            var directory = _directoryService.GetCurrentDirectory();

            var builder = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("appsettings.json", false, false);

            Configuration = builder.Build();
        }

        // You can give your plugin a friendly name to make it
        // easier to troubleshoot in the Framework Runtime
        // if you experience issues.
        public string Name => "My Igor Plugin";

        public IConfiguration Configuration { get; private set; }
    }
}
