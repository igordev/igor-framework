﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Igor.Framework.Services;
using Igor.Gateway.Api.Sdk.Apis.Spaces;
using Microsoft.Extensions.Logging;

namespace Tasks
{
    // Tasks are intended for operations that need executed
    // on a given frequency. For example, if you need to
    // run some code every 15 seconds.
    public class MyTask : ITask
    {
        private readonly ILogger _logger;
        private readonly ISpaceService _spaceService;

        public MyTask(ILogger logger, ISpaceService spaceService)
        {
            _logger = logger;
            _spaceService = spaceService;
        }

        // Frequency in which to execute your code.
        // The runtime will enforce a floor of 10 seconds if
        // this value is not provided or is less than 10.
        public uint ExecutionFrequencySeconds => 15;

        // The amount of time to wait before first executing
        // the task after the runtime starts.
        public uint StartupDelaySeconds => 0;

        public async Task Run(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Task is executing!");

            // In previous examples, we've always been sure to return
            // a Task by using the following line:
            // return Task.CompletedTask;
            //
            // Alternatively, you can make your method asynchronous by
            // using the async keyword and awaiting an async method

            if (DateTime.Now.Year == 2018)
                await _spaceService.TurnOff(1);
            else
                await _spaceService.TurnOn(1);
        }
    }
}
