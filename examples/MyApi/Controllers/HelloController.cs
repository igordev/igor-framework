﻿using Igor.Framework.Web;
using Microsoft.AspNetCore.Mvc;

namespace MyApi.Controllers
{
    // You can host your own API within the Igor Framework.
    // There is no limit to the number of controllers you can
    // have and works very similar to ASP.NET Core. In fact,
    // it requires that you reference the Microsoft.AspNetCore.Mvc (v2.1.1)
    // NuGet package.
    //
    // Your plugin receives its own unique root route when the
    // runtime starts up. The template for plugin web routes is
    // (default port is 5000):
    // http://<ip address>:5000/igor/plugins/[name of your plugin minus spaces]/[your route]
    //
    // This example would be found at the following route:
    // http://<ip address>:5000/igor/plugins/myigorplugin/api/hello/world
    //
    // You can see a list of discovered routes the runtime sees by going to
    // the following address in your browser:
    // http://<ip of runtime>:5000/igor/framework/v1/api/diagnostics/routes
    //
    // The [IgorFrameworkRoute] attribute is required at the class level
    // to tell the runtime that this controller should be registered with the framework. 
    // It behaves the same way as the default ASP.NET Core RouteAttribute.
    //
    // HTTP methods cannot be implied within the framework and must be specified
    // using ASP.NET Core attributes, such as [HttpGet], [HttpPost], etc.
    [ApiController]
    [IgorFrameworkRoute("api/[controller]")]
    public class HelloController : Controller
    {
        [HttpGet]
        [Route("world")]
        public string Get()
        {
            return "Hello world!";
        }
    }
}
