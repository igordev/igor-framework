﻿using System.Threading.Tasks;
using Igor.Framework;
using Igor.Framework.WebHooks;
using Igor.Framework.WebHooks.ActionSets;
using Igor.Framework.WebHooks.Schedules;

namespace EventListeners.Multi
{
    // In this example you can see that Event handlers share the 
    // same Handle method. Unlike the MultiDataHandler.cs example,
    // this Handle method will be called if either an Action Set 
    // Executed event or Schedule Occurred event are fired within
    // the Igor Gateway Software.
    // It is up to your code to distinguish between the two if
    // necessary.
    public class MultiEventHandler : IActionSetExecutedEventHandler, IScheduleOccurredEventHandler
    {
        public Task Handle(IEvent<WebHookNotification> @object)
        {
            // Execute your code here

            // Be sure to return a task
            return Task.CompletedTask;
        }
    }
}