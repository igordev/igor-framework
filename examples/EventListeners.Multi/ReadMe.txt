﻿This example demonstrates how to use multiple web hook event handlers on a single class when using the Igor Framework.

This project will register an Action Set Executed and Scheduled Occurred web hooks with the Igor Gateway Software and will call both the MultiDataHandler and MultiEventHandler when an Action Set Executed or Schedule Occurred event is triggered in the gateway.