﻿using System.Threading.Tasks;
using Igor.Framework;
using Igor.Framework.WebHooks;
using Igor.Framework.WebHooks.ActionSets;
using Igor.Framework.WebHooks.Schedules;
using Igor.Gateway.Dtos.ActionSets;
using Igor.Gateway.Dtos.Schedules;

namespace EventListeners.Multi
{
    // The Igor Framework supports the ability to add multiple handlers
    // to the same class as you can see in this example.
    // Only the Handle method for the firing event will be called
    // when the corresponding event is fired within the Igor Gateway
    // Software.
    public class MultiDataHandler : IActionSetExecutedDataHandler, IScheduleOccurredDataHandler
    {
        public Task Handle(IEventData<ActionSetDto, WebHookNotification> @object)
        {
            // Execute your code here

            // Be sure to return a task
            return Task.CompletedTask;
        }

        public Task Handle(IEventData<ScheduleDto, WebHookNotification> @object)
        {
            // Execute your code here

            // Be sure to return a task
            return Task.CompletedTask;
        }
    }
}
