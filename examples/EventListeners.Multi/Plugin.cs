﻿using Igor.Framework;
using Microsoft.Extensions.Configuration;

namespace EventListeners.Multi
{
    public class Plugin : IPlugin
    {
        public void Initialize()
        {
            // nothing to initialize
        }

        public string Name => "My Multi-Event Listener Plugin";

        public IConfiguration Configuration { get; }
    }
}
