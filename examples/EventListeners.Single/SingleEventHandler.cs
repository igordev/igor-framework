﻿using System.Threading.Tasks;
using Igor.Framework;
using Igor.Framework.WebHooks;
using Igor.Framework.WebHooks.ActionSets;

namespace EventListeners.Single
{
    // An event handler will give you the notification
    // immediately as it receives it from the gateway.
    // This is similar to if you were to write your own
    // application to listen for web hook events sent
    // directly from the gateway.
    //
    // Most event handlers (indicated by the word Event
    // in the interface name) have a "twin" interface
    // lacking the word Event that will also fetch the
    // object for you. See SingleDataHandler.cs for
    // an example.
    public class SingleEventHandler : IActionSetExecutedEventHandler
    {
        public Task Handle(IEvent<WebHookNotification> @object)
        {
            // Execute your code here

            // Be sure to return a task
            return Task.CompletedTask;
        }
    }
}