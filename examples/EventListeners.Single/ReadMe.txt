﻿This example demonstrates how to use web hook event handlers within the Igor Framework.

This project will register an Action Set Executed web hook with the Igor Gateway Software and will call both the SingleDataHandler and SingleEventHandler when an Action Set Executed event is triggered in the gateway.