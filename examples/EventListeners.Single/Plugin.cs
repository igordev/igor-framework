﻿using Igor.Framework;
using Microsoft.Extensions.Configuration;

namespace EventListeners.Single
{
    public class Plugin : IPlugin
    {
        public void Initialize()
        {
            // nothing to initialize
        }

        public string Name => "My Single Event Listener Plugin";

        public IConfiguration Configuration { get; }
    }
}
