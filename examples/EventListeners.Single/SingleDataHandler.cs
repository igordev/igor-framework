﻿using System.Threading.Tasks;
using Igor.Framework;
using Igor.Framework.WebHooks;
using Igor.Framework.WebHooks.ActionSets;
using Igor.Gateway.Dtos.ActionSets;

namespace EventListeners.Single
{
    // A "non-Event" handler (indicated by the lack of the
    // word Event in the interface name) behaves the same
    // way as a normal Event handler with the addition 
    // that it will go out and fetch the object that 
    // triggered the event (if possible) before calling 
    // Handle on your handler. 
    // This does add a small delay before you code is 
    // executed.
    public class SingleDataHandler : IActionSetExecutedDataHandler
    {
        public Task Handle(IEventData<ActionSetDto, WebHookNotification> @object)
        {
            // Execute your code here

            // Be sure to return a task
            return Task.CompletedTask;
        }
    }
}