IF /I "%APPVEYOR_REPO_BRANCH%"=="master" (
    call dotnet pack .\src\Igor.Framework\Igor.Framework.csproj --no-restore
) ELSE IF /I "%APPVEYOR_REPO_BRANCH%"=="develop" (
    call dotnet pack .\src\Igor.Framework\Igor.Framework.csproj --no-restore --version-suffix "prerelease%APPVEYOR_BUILD_NUMBER%"
) ELSE (
    call dotnet pack .\src\Igor.Framework\Igor.Framework.csproj --no-restore --version-suffix "prerelease%APPVEYOR_REPO_BRANCH%%APPVEYOR_BUILD_NUMBER%"
)